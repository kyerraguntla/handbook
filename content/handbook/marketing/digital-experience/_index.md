---

title: Digital Experience Handbook
description: >-
  Learn more about the Digital Experience purpose, vision, mission, objective
  and more in this handbook.
---

## Overview

### 🙌 Purpose

*Why we exist*

We take a customer-centric approach to educating prospects on how GitLab enables them to deliver software faster and more securely.

## Team Members

| Role | Name |
|--|--|
| Senior Product Designer | [Tina Lise Ng](https://gitlab.com/Tinaliseng) |
| Senior Product Designer |[Trevor Storey](https://gitlab.comtrevor-storey) |
| Product Design | [Carrie Tsang](https://gitlab.com/ctsang-ext) |
| Senior Frontend Engineer| [Megan Filo](https://gitlab.com/meganfilo) |
| Frontend Engineer | [Javi Garcia](https://gitlab.com/jgarc)|
| Senior Frontend Engineer | [Laura Duggan](https://gitlab.com/lduggan) |
| Senior Frontend Engineer | [Marg Mañunga](https://gitlab.com/mmanunga-ext) |
| Frontend Engineer | [Miguel Duque](https://gitlab.com/mduque-ext) |
| Senior Frontend Engineer | [Nathan Dubord](https://gitlab.com/ndubord) |
| Fullstack Engineer | [Miracle Banks](https://gitlab.com/miraclebanks) |
| Senior Fullstack Engineer | [John Arias](https://gitlab.com/jariasc-ext) |
| Senior Fullstack Engineer | [Mateo Penagos](https://gitlab.com/mpenagos-ext) |
| Senior Product Manager | [Filza Qureshi](https://gitlab.com/fqureshi)|
| Engineering Manager | [Lauren Barker](https://gitlab.com/laurenbarker) |

## Scope

Our team leads the GitLab's digital marketing platform, or simply the “Marketing Site" refers to `https://about.gitlab.com`.
**We own the following repositories:**
- [Buyer Experience](https://gitlab.com/gitlab-com/marketing/digital-experience)
- [GitLab Blog](https://gitlab.com/gitlab-com/marketing/digital-experience/gitlab-blog)
- [Navigation](https://gitlab.com/gitlab-com/marketing/digital-experience/navigation)
- [Slippers Design System](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui)

**Our team strengths & core capabilities:**
- Engineering and UX desing for about.gitlab.com
- Cross collaboration
- Speed and delivery
- Customer journey maps

**Our team supports:**
- HTML email templates
- PathFactory
- Marketo
- learn.gitlab.com
- [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com)
- Globalization

**We do not support:**
- [Handbook](https://gitlab.com/gitlab-com/content-sites/handbook)
- [Internal handbook](https://gitlab.com/gitlab-com/content-sites/internal-handbook)
- Content copwriting or edits
- Cloudflare server infrastructure

**Teams we work closely with:**
- SEO
- Analytics
- Product Marketing
- Content Moarketing
- Brand Strategy
- Marketing Ops
- Blog
- Globalization
- Events
- Competitve Intelligence

## OKRs

We collaboratively define OKRs as a team with cross functional partners in advance of each quarter. Once OKR candidates are complete we review, size/scope them and align on which best help achieve our business objectives.

### Current Quarterly Plan

[FY25Q1 Digital Experience Quarterly Plan & OKRs](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/367)

## Iteration Process

We release every 2 weeks, always on a Wednesday. We can push MRs at any time but for collaborative work initiatives, we plan a package for delivery to ensure we’re consistently improving our prospective customer’s experience.

We start iteration a Monday. The following Wednesday is async release day, although we are releasing throughout the iteration. On the thursday a week after our Monday iteration planning, we come together for a syncrornous meeting to share what we've released. This meetings are recorded and can be viewed in our [#digita-experience YouTube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0KrakNGW0ruM5UL7DDlrMBba).

<figure class="video_container">
<iframe src="https://calendar.google.com/calendar/embed?src=c_g97ibfb3lq183mphm8mnbjfk34%40group.calendar.google.com&ctz=America%2FDenver" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>
</figure>

## Issue Board
- [Digital Experience Board](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/boards/3288685)

- [Current DEX Group Conversion Iteration Board](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/boards/7320189?iteration_id=Current&label_name[]=dex-group%3A%3Aconversion)
- [Current DEX Group Optimization Iteration Board](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/boards/5955968?iteration_id=Current&label_name[]=dex-group%3A%3Aoptimization)

### Labels and Workflow Boards

We use issue boards to track issue progress throughout a iteration. Issue boards should be viewed at the highest group level for visibility into all nested projects in a group.


The Digital Experience team uses the following issue labels for distinguishing ownership of issues between specialities:

| Who          | Title       |
| ----------    | -----------|
| User Experience| ~dex::ux     |
| Engineering| ~dex::engineering     |

The Digital Experience team uses the following labels for tracking merge request rate and ownership of issues and merge requests.

| What & Current Issues | Label                   |
| ----------    | -----------             |
| [Work to be triaged](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/issues/?sort=created_date&state=opened&label_name%5B%5D=dex-status%3A%3Atriage&first_page_size=100)| `~"dex-status::traige"`   |
| [Refinement on issue is needed](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/issues/?sort=created_date&state=opened&label_name%5B%5D=dex-status%3A%3Arefinement&first_page_size=100)| `~"dex-status::refinement"`   |
| [Issues in the backlog](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/issues/?sort=created_date&state=opened&label_name%5B%5D=dex-status%3A%3Abacklog&first_page_size=100) | `~"dex-status::backlog"`   |
| [Issues to be worked on](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/issues/?sort=created_date&state=opened&label_name%5B%5D=dex-status%3A%3Atodo&first_page_size=100)| `~"dex-status::to-do"`   |
| [Currently being actioned](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/issues/?sort=created_date&state=opened&label_name%5B%5D=dex-status%3A%3Adoing&first_page_size=100)| `~"dex-status::doing"`
| [Work in review](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/issues/?sort=created_date&state=opened&label_name%5B%5D=dex-status%3A%3Areview&first_page_size=100)| `~"dex-status::review"`  |
| [Unplanned work](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/issues/?sort=created_date&state=opened&label_name%5B%5D=dex-unplanned&first_page_size=100)| `~"dex-unplanned"`  |
| [Issue for Conversion team to complete](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/issues/?sort=created_date&state=opened&label_name%5B%5D=dex-group%3A%3Aconversion&first_page_size=100) |  `~"dex-group::conversion"` |
| [Issue for Optmization team to complete](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/issues/?sort=created_date&state=opened&label_name%5B%5D=dex-group%3A%3Aoptimization&first_page_size=100) |  `~"dex-group::optimization"` |
| [Issue for product designer to complete](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/issues/?sort=created_date&state=opened&label_name%5B%5D=dex%3A%3Aux&first_page_size=100) |  `~"dex::ux"` |
| [Issue for engineer to complete](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/issues/?sort=created_date&state=opened&label_name%5B%5D=dex%3A%3Aengineering&first_page_size=100) |  `~"dex::engineering"` |



Digital Experience teams work across the GitLab codebase on multiple groups and projects including:
- The [gitlab.com/gitlab-org](https://gitlab.com/gitlab-org/) group
- [gitlab](https://gitlab.com/gitlab-org/gitlab)
- [customers-gitlab-com](https://gitlab.com/gitlab-org/customers-gitlab-com)
- The [gitlab.com/gitlab-com](https://gitlab.com/gitlab-com/) group
- [www-gitlab-com](https://gitlab.com/gitlab-com/www-gitlab-com)
- [Buyer Experience](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience)
- [GitLab Blog](https://gitlab.com/gitlab-com/marketing/digital-experience/gitlab-blog)
- [Slippers Design System](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui)
- [Marketing Site Navigation Package](https://gitlab.com/gitlab-com/marketing/digital-experience/navigation)

### Estimation

Before work can begin on an issue, we should estimate it first after a preliminary investigation. This is normally done in the iteration planning meeting.

| Weight | Description (Engineering) |
| ------ | ------ |
| 1 | The simplest possible change. We are confident there will be no side effects. |
| 2 | A simple change (minimal code changes), where we understand all of the requirements. |
| 3 | A simple change, but the code footprint is bigger (e.g. lots of different files, or tests effected). The requirements are clear. |
| 5 | A more complex change that will impact multiple areas of the codebase, there may also be some refactoring involved. Requirements are understood but you feel there are likely to be some gaps along the way. |
| 8 | A complex change, that will involve much of the codebase or will require lots of input from others to determine the requirements. |
| 13| A significant change that may have dependencies (other teams or third-parties) and we likely still don't understand all of the requirements. It's unlikely we would commit to this in a milestone, and the preference would be to further clarify requirements and/or break in to smaller Issues. |

In planning and estimation, we value [velocity over predictability](/handbook/engineering/development/principles/#velocity-over-predictability). The main goal of our planning and estimation is to focus on the [MVC](/handbook/values/#minimal-viable-change-mvc), uncover blind spots, and help us achieve a baseline level of predictability without over optimizing. We aim for 70% predictability instead of 90%. We believe that optimizing for velocity (merge request rate) enables our Growth teams to achieve a [weekly experimentation cadence](/handbook/product/growth/#weekly-growth-meeting).

- If an issue has many unknowns where it's unclear if it's a 1 or a 5, we will be cautious and estimate high (5).
- If an issue has many unknowns, we can break it into two issues. The first issue is for research, also referred to as a [Spike](https://en.wikipedia.org/wiki/Spike_(software_development)), where we de-risk the unknowns and explore potential solutions. The second issue is for the implementation.
- If an initial estimate is incorrect and needs to be adjusted, we revise the estimate immediately and inform the Product Manager. The Product Manager and team will decide if a milestone commitment needs to be adjusted.

### Triage
The purpose of the traiage meeting is to create a list of refined issues that meet our current goals. This list will include a combination of bugs, features, and optimizations. These issues are manually added to the next iteration until the desired weight point limit is reached. Refinement is completed async by before to ensure issues are prepared for upcoming iterations. This involves deleting obsolete/duplicate issues, adding missing context/labels, and moving issues to either the backlog, or further refinement. Keeping the backlog organized is a must, it eliminates clutter and creates cohesion between issues. Enabling the team to navigate and contribute more efficiently.

**Cadence:** 25min, bi-weekly (zoom)

**Who:** Engineering representative, Product management. [Triage Agenda](https://docs.google.com/document/d/15t-Ke_c6uMdzrcqUj6P01lYQgHSXcgerb5ShUtADLr8/edit?usp=sharing).
**What**:
* Review backlog for iteration candidates.
* Populate the next iteration with prioritized issues.
* Move issues to refinement/backlog.
* Close obsolete/duplicate issues.
* Assign labels:
    * dex-status
    * dex-group
    * dex-engineering/dex-design
* Assign weight points.

### Planning (Iteration Plan Sync)
Iteration planning is an event that kicks off the start of an iteration. The purpose of the meeting is to collaboratively spread the prioritized list of issues amongst the team. These meetings are recorded and uploaded to our [Digital Experience playlist on GitLab Unfiltered](https://www.youtube.com/playlist?list=PL05JrBw4t0KrakNGW0ruM5UL7DDlrMBba).

**Cadence:** 25min, bi-weekly (zoom)

**What:**
* The team distributes the prioritized list of issues.
* Communicate timelines, dependencies, etc.


### Release
An event to showcase what the team has accomplished over the past iteration. These meetings are recorded and uploaded to our [Digital Experience playlist on GitLab Unfiltered](https://www.youtube.com/playlist?list=PL05JrBw4t0KrakNGW0ruM5UL7DDlrMBba).

**When:** Thursdays, 25min, bi-weekly (zoom)

**What:**
* Showcase what has been released in the past iteration.


### Retrospective
The retrospective is an event held at the end of an iteration, used to discuss what went well, and what can be improved on. An ongoing agenda can be found [here](https://docs.google.com/document/d/1kMNiUF2UDuSrMDuzLyRi8OEhVxry_MJoYi38RmmWafY/edit?usp=sharing). This meeting is recorded and uploaded to our [Digital Experience playlist on GitLab Unfiltered](https://www.youtube.com/playlist?list=PL05JrBw4t0KrakNGW0ruM5UL7DDlrMBba).

**When:** Thursdays, 40min, bi-weekly (zoom)

**What:**
* Discuss what went well and what can be improved on.
* Communicate any process changes, etc. This is the only meeting where the whole team is present.

### Iteration Changelogs

At the end of every iteration we run a scheduled pipeline job that generates a [changelog for the Buyer Expeirence repository](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/blob/main/CHANGELOG.md). It shows all the chnages made to the project with semanitc commits.

## **FAQ:**
### Iteration boards
**How long is an iteration?**

An iteration is 2 weeks, running from Monday to the following Thursday.

**Where can I find the iteration boards?**

Iteration boards are created at the [team level](https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/boards/3288486?iteration_id=Current&_gl=1*xlvfjl*_ga*MTMyODM2NjQzOC4xNjU3OTA0NDQ2*_ga_ENFH3X7M5Y*MTY2MzgwNzk2NC4xMDMuMS4xNjYzODA4NDg1LjAuMC4w), and the individual level:

[Digital Experience](https://gitlab.com/gitlab-com/marketing/digital-experience) > Issues - Boards > Then selecting an individual's name or group from the dropdown.

**What are the iteration boards used for?**

Iteration boards are meant to give an overview of what the team is working on, and to provide a rough idea on what the team is capable of producing in an iteration.

**How do I move issues from start to finish?**

At the start of an iteration, all issues will have the dex-status::todo label. As issues are worked on, the dex-status label will need to be updated. This can be done by dragging (on your individual board) between columns, or manually changing the dex-status label on the issue.

**What if I wasn’t able to complete my iteration board?**

Don’t stress, weight points are estimates, unforeseen events happen. Any carryover can be added to the next iteration.

**What if I complete my iteration board early?**

A few options for when an an individual's iteration board is complete:
1. Offer assistance to other team members.
2. Pull a new issue from the backlog into the current iteration.
3. Sharpen skills/tools.
4. General housekeeping.


### Weight points
**What is a weight point?**

A weight point is a unit of measurement that’s used to develop a rough estimate of the work required to complete an issue. 1 weight point is measured as .5 days.

**How many weight points should an issue be?**

The suggested task duration is between 2-4 weight points (1-2 days). There will be exceptions, but it’s recommended to break issues into smaller units of work. Small units of work allow for quicker review cycles, and facilitates collaboration.


### Issues
**What should I do if I’m assigned new issues mid-iteration?**

Generally if an issue is added mid-iteration, it's high priority. It’s recommended to work with your team to remove the same amount of weight points from your iteration to make room. These removed issues should go back in the backlog.

Apply the `dex-unplanned` label.



**Do I need to add any labels?**

Before entering an iteration, an issue should already be refined with the proper labels. The only label that changes is the [dex-status](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/labels?subscribed=&search=dex-status) label (as the issue moves from start to finish).

**What if I’m assigned an issue that I can’t close due to content/data gathering?**

Unfortunately there will always be edge case issues that cannot be resolved in an iteration To mitigate the amount of carryover, it’s recommended to break the issue into smaller chunks

Example A: If an issue is open while waiting on content/assets, it’s best to create a content/asset gathering issue and close the original issue.

Example B: An issue is open while gathering data from an AB test, it may be best to create an issue to start the information gathering, and an issue to analyze the data at the end.

## Weekly Check In

We use [Geekbot](https://geekbot.com/) to conduct asynchronous, weekly check-ins on iteration progress.

Each member of the Digital Experience team should be listed as a participant in the weekly check ins, and everyone should have permissions to manage the application for our team. The app can be configured through the [Geekbot Dashboard](https://app.geekbot.com/dashboard/), which you can visit directly, or find by clicking the **Geekbot** Slack conversation, navigating to the **About** tab, and clicking **App Homepage**.

## Production Change Lock (PCL)

Similar to [the engineering department](/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl), we sometimes temporarily halt production changes to [the Buyer Experience repository](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience) when team availability is reduced, or we expect atypical user behavior (such as during high-visibility public events).

Risks of making a production environment change during these periods includes immediate customer impact and/or reduced engineering team availability in case an incident occurs. Therefore, we have introduced a mechanism called Production Change Lock (PCL). We are listing the events here so that teams are aware of the PCL periods.

The following dates are currently scheduled PCLs. Times for the dates below begin at 09:00 UTC and end the next day at 09:00 UTC.

| Dates | Reason |
| --- | --- |
| 2023-12-22 to 2024-01-02 | End of 2023, limited coverage |

During PCL periods, merge requests and deployments can only be made by senior team members, managers, and levels of management above our team.

## Figma Process

- [How we use Figma](/handbook/marketing/digital-experience/figma/)

## GitLab Product Process

From time to time, our team has objectives that require us to collaborate on the [GitLab product](https://gitlab.com/gitlab-org/gitlab). [Read more about the process for our engineers to onboard](/handbook/marketing/digital-experience/engineering-gitlab-product)

**Special cases during release post schedule:** we hold off on making changes to the `www-gitlab-com` repository during [release post](/handbook/marketing/blog/release-posts/#release-posts) days. The release post process is handled by a different team, and it can be disruptive to their work when we release changes to dependencies, CI/CD, or other major changes around their monthly release cadence.

## Repository Health Contributions

At the end of every sprint cycle, Digital Experience team members can spend 10% or one day to work on issues related to improving the health of about.gitlab.com, the developer experience, tackle tech debt, or improve our documentation.

The structure of Repository Health Day is as follows:
1. Team members will choose what they wish to work on for this day.
1. Each team member will submit a single merge request to the [Slippers Design System](https://gitlab.com/gitlab-com/marketing/digital-experience/slippers-ui), [Navigation](https://gitlab.com/gitlab-com/marketing/digital-experience/navigation), or [Buyer Experience](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience) repository by the end of repository health day.
1. This merge request will be related to an issue from any partner or group within GitLab.

By allowing our team members to contribute to the health of our repositories for a day, we can contribute low-effort, high-impact solutions that will drive results for our team, partners, and the entire marketing site. This will enable Digital Experience team members to use their strengths to efficiently drive results for https://about.gitlab.com/. We’re all good at different things and come from different backgrounds. Let’s use that to our advantage to build a better tech stack that is inclusive of the team members that use it everyday.

## Analytics

For any Digital Experience analytics request, please create an issue within the [Marketing Strategy and Analytics](https://gitlab.com/gitlab-com/marketing/marketing-strategy-performance/-/issues/new) project using the `dex_analytics_request` template to outline specific requirements. To ensure a smooth milestone planning, please assign the issue to [@dennischarukulvanich](https://gitlab.com/dennischarukulvanich) ideally a week or more in advance.

##  Sales Shadows
### How to set up a Sales Shadow

#### SMB

1. Contact a Sales Development Manager ([Josh Downey](https://gitlab.com/joshdowney)) or Director, Sales Development ([Brian Tabbert](https://gitlab.com/btabbert)).
2. Let them know what team you’re from and that you’d like to shadow a few sales calls to observe real GitLab prospects talking to our Sales team to learn [insert what you’re trying to learn here. Example: what the common topics potential customers want to discuss with our Sales team are.]
3. Inform the Sales Development Manager or Director, Sales Development how many shadows you’d like to do and a rough timeline for when you’d like to do them.
4. The Sales Development Manager or Director, Sales Development will inform their Sales Development Reps (SDRs), and they will add you to relevant, upcoming Discovery calls with an Account Executive (AE).
5. Accept the invite and review any supplied material when you add it to your calendar.
6. When joining the call, remember:
    1. You’re there to observe. If asked to introduce yourself, come off mute and do so, then go back on mute and let the Sales team do what they do.
    2. Keep your camera on.
    3. Have a notes doc prepared and take notes on your observations and insights.
7. After the call, review your notes, and synthesize and create action items.
8. Send a thank you message to the Sales team members who hosted you.
9. Once all shadows are completed, share your notes and insights with the team.

#### Team Shadow Expectations

Whoever gets closest to the customer wins. With this in mind, the Digital Experience team is expected to shadow Sales calls regularly.

## Contact Us

### Slack Group

**@digital-experience** use this handle in any channel to mention every member of our team.

### Slack Channels

[#digital-experience-team](https://gitlab.slack.com/archives/CN8AVSFEY)

### Slack Application

We have created a Slack application called Dex Bot to notify our team about important CMS changes, read more about it [here](/handbook/marketing/digital-experience/dexbot/)

## GitLab Unfiltered Playlist

Watch our team in action on YouTube!

[Digital Experience](https://www.youtube.com/playlist?list=PL05JrBw4t0KrakNGW0ruM5UL7DDlrMBba)


## Requesting Support

### Things we don't do

1. **Content changes**. You can do these yourself using our CMS, Contentful:
    1. [Here's a quick video](https://www.youtube.com/embed/6pdXijDzYkg?si=8JbdUBmzPHcWgzTk) on how to search for and edit existing content for the marketing site. For completely new pages, please fill out an [issue](/handbook/marketing/digital-experience/#issue-template-to-submit-an-idea-to-drive-our-business-goals)
    1. Want to learn more about our Contentful CMS? [Here's the documentation](/handbook/marketing/digital-experience/contentful-cms)
2. **Create content**. You can collaborate with our excellent [Global Content team](/handbook/marketing/brand-and-product-marketing/content/) for these needs.
3. **Create branded assets, custom graphics, illustrations**. Our [Brand design team](/handbook/marketing/brand-and-product-marketing/design/) is so good at this, you definitely want their expertise.

### Issue template to submit an idea to drive our business goals

We love collaborating on work that drives our North Star and supporting metrics. If you have an idea, a strategic initiative, or an OKR that we requires our support here's how you can kick off our collaboration:

1. Review the FAQ section related to pre-work that will increase the chances your issue is prioritized.
2. Create an issue using [this template](https://gitlab.com/gitlab-com/marketing/digital-experience/buyer-experience/-/issues/new#)

### DEX team members with platform access
<details>
    <summary>LaunchDarkly</summary>
    <li>@dcharukulvanich</li>
    <li>@fqureshi</li>
    <li>@jgarc</li>
    <li>@lduggan</li>
    <li>@mpenagos-ext</li>
    <li>@meganfilo</li>
    <li>@mpreuss</li>
    <li>@miraclebanks</li>
    <li>@ndubord</li>
</details>

## Digital Experience FAQ

<details>
    <summary>Previous <b>Quarterly Plans & OKRs</b></summary>
    <li><a href="https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/367">FY25Q1 Digital Experience Quarterly Plan & OKRs</a></li>
    <li><a href="https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/342">FY24Q24 Digital Experience Quarterly Plan & OKRs</a></li>
    <li><a href="https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/309">FY24Q3 Digital Experience Quarterly Plan & OKRs</a></li>
    <li><a href="https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/251">FY24Q2 Digital Experience Quarterly Plan & OKRs</a></li>
    <li><a href="https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/210">FY24Q1 Digital Experience Quarterly Plan & OKRs</a></li>
    <li><a href="https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/175">FY23Q4 Digital Experience Quarterly Plan & OKRs</a></li>
    <li><a href="https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/136">FY23Q3 Digital Experience Quarterly Plan & OKRs</a></li>
    <li><a href="https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/107">FY23Q2 Digital Experience Quarterly Plan & OKRs</a></li>
    <li><a href="https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/85">FY23Q1 Digital Experience Quarterly Plan & OKRs</a></li>
    <li><a href="https://gitlab.com/groups/gitlab-com/marketing/digital-experience/-/epics/3">FY22Q4 Digital Experience Quarterly Plan & OKRs</a></li>
    <li><a href="https://gitlab.com/groups/gitlab-com/marketing/inbound-marketing/-/epics/385">FY22Q3 Digital Experience Quarterly Plan & OKRs</a></li>
</details>
<details>
    <summary>Content Wireframe Instructions</summary>
    The Digital Experience team is primarily responsible for facilitating content, not creating it. Please prepare a content plan:
    <li>Provide the layout you think would work best from existing pages or existing blocks</li>
    <ul>
      <li>You can use the <a href="https://www.figma.com/file/nWIOpmuMp7RZXmfTj6ujAF/Slippers_foundations?node-id=1280%3A62">Blocks section in Slippers</a> to "shop" for blocks and plan your content to work with designs that already exist.</li>
      <li>This youtube video shows <a href="https://www.youtube.com/watch?v=HbnxXE4PT_s">how to navigate the Slippers Figma file</a> and how a content doc can work with the blocks.</li>
      <li>Not sure what a "block" is? You can read about that and more on our <a href="https://about.gitlab.com/handbook/marketing/digital-experience/digital-definitions/#blocks">Digital Definitions</a> handbook page.</li>
    </ul>
    <li>Provide the content in the layout of the existing block or page template</li>
    <ul>
      <li>Use this <a href="https://docs.google.com/document/d/1c2OYCtT57SACYSt55YMB5ZW-rt-JFw5RxYNWKPZt-Ao/edit?usp=sharing">Content + DSN Block Template</a> as a base. This template allows you to input <a href="https://www.figma.com/file/nWIOpmuMp7RZXmfTj6ujAF/Slippers_foundations?node-id=1280%3A62">existing block designs</a> mentioned above.</li>
      <li>This <a href="https://docs.google.com/document/d/1ypPJVNEpaSiafY0BnZ0p49tBfVeBGQnySVhgYE7gikk/edit?usp=sharing">Content Wireframe Template</a> is based on the current about.gitlab.com/enterprise page</li>
    </ul>

</details>
<details>
    <summary>Image Requirements</summary>
    <ul>
      <li>See <a href="https://about.gitlab.com/handbook/marketing/digital-experience/image-guidelines/">image guidelines</a> for specs</li>
    </ul>
</details>
<details>
    <summary>SEO Requirements</summary>
    <li>Know the URL and keywords you want to use</li>
    <ul>
      <li>SEO and keyword analysis from the Search Team <a href="https://gitlab.com/gitlab-com/marketing/inbound-marketing/growth/issues/new?issuable_template=keyword-research-request">Issue Templates</a> is recommended.</li>
      <li>See <a href="https://about.gitlab.com/handbook/marketing/digital-experience/website/#naming-conventions">naming conventions</a></li>
    </ul>
</details>
